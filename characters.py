from random import randint, random
from skills import CriticalStrike, Resilience


class Character:
    def __init__(self, name, health_range, strength_range, defence_range, speed_range, luck_range):
        self.name = name
        self.health = randint(*health_range)
        self.strength = randint(*strength_range)
        self.defence = randint(*defence_range)
        self.speed = randint(*speed_range)
        self.luck = randint(*luck_range) / 100
        self.skills = []

    def skill_modifier(self, skill_type, base_logs):
        for skill in self.skills:
            if skill.skill_type == skill_type and (multiplier := skill.apply(self, base_logs)):
                return multiplier
        return None

    def calculate_damage(self, base_logs):
        if damage_modifier := self.skill_modifier("attack", base_logs):
            return self.strength, damage_modifier

        base_logs.append(f"{self.name} landed a base attack!")
        return self.strength, 1

    def take_damage(self, damage, base_logs):
        damage = damage - self.defence

        if defence_modifier := self.skill_modifier("defence", base_logs):
            damage = int(damage * defence_modifier)

        self.health -= damage

        base_logs.append(f"{self.name} gets {damage} damage!")

    def is_lucky(self):
        return random() < self.luck

    def current_health(self):
        return self.health if self.health > 0 else 0

    def stats(self):
        return {
            "strength": self.strength,
            "defence": self.defence,
            "speed": self.speed,
            "luck": self.luck
        }


class Hero(Character):
    def __init__(self):
        super().__init__("Hero", (70, 100), (70, 80), (45, 55), (40, 50), (10, 30))
        self.skills.append(CriticalStrike())
        self.skills.append(Resilience())


class Villain(Character):
    def __init__(self):
        super().__init__("Villain", (60, 90), (60, 90), (40, 60), (40, 60), (25, 40))
