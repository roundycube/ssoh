from fastapi import FastAPI, Request, WebSocket, WebSocketDisconnect
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from gameplay import Battle

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


@app.get("/")
def index(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


class WebSocketBattleManager:
    def __init__(self):
        self.connections = set()
        self.battle = None

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.connections.add(websocket)
        if len(self.connections) == 1:
            await self.start_battle()

    async def disconnect(self, websocket: WebSocket):
        self.connections.remove(websocket)
        if len(self.connections) == 0:
            await self.start_battle()

    async def start_battle(self):
        self.battle = Battle()
        await self.send_data(self.battle.send_logs())

    async def send_data(self, data: dict):
        for connection in self.connections:
            await connection.send_json(data)

    async def process_attack(self):
        self.battle.attack(self.battle.hero, self.battle.villain)
        if self.battle.villain.health > 0:
            self.battle.attack(self.battle.villain, self.battle.hero)
        await self.send_data(self.battle.send_logs())


battle_manager = WebSocketBattleManager()


@app.websocket("/battle")
async def battle_websocket(websocket: WebSocket):
    await battle_manager.connect(websocket)
    try:
        while True:
            data = await websocket.receive_text()
            if data == "attack":
                await battle_manager.process_attack()
            elif data == "reset":
                await battle_manager.start_battle()
    except WebSocketDisconnect:
        await battle_manager.disconnect(websocket)

