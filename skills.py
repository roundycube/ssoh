from random import random


class Skill:
    def __init__(self, name, skill_type, chance):
        self.name = name
        self.skill_type = skill_type
        self.chance = chance / 100


class CriticalStrike(Skill):
    def __init__(self):
        super().__init__("Critical Strike", "attack", 10)

    def apply(self, attacker, logs):
        if random() <= self.chance:
            if random() <= 0.01:
                logs.append(f"{attacker.name} got extremely lucky and struck three times!")
                return 3
            logs.append(f"{attacker.name} got lucky and landed a critical strike!")
            return 2


class Resilience(Skill):
    def __init__(self):
        super().__init__("Resilience", "defence", 20)
        self.available = True

    def apply(self, defender, logs):
        if self.available and random() <= self.chance:
            logs.append(f"{defender.name} activated Resilience and took only half the damage!")
            self.available = False
            return 0.5
        self.available = True
        return 1
