# SSOH



## About

This is a Tech Task solution for softbinator.com

## Project Setup

```
clone this project
cd to this project folder

! don't forget to create and activate you virtual environment

install requirements:
 - pip install -r requirements.txt

run project:
 - uvicorn main:app --reload

to access the SSOH solution, visit:
 - http://127.0.0.1:8000

run tests:
 - pytest tests
```
