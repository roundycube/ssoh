import pytest
from fastapi.testclient import TestClient
from main import app


@pytest.fixture(scope="session")
def client():
    return TestClient(app)


@pytest.fixture(scope="session")
def websocket_client(client):
    with client.websocket_connect("/battle") as websocket:
        return websocket


def test_index(client):
    response = client.get("/")
    assert response.status_code == 200


def test_websocket_connect(websocket_client):
    with websocket_client as websocket:
        response = websocket.receive_json()
        assert "log" in response
        assert "hero_health" in response
        assert "villain_health" in response
        assert "game_over" in response


def test_process_attack(websocket_client):
    with websocket_client as websocket:
        initial_response = websocket.receive_json()

        websocket.send_text("attack")
        response = websocket.receive_json()

        assert response["log"] != initial_response["log"]

        websocket.send_text("attack")
        response = websocket.receive_json()

        assert response["log"] != initial_response["log"]


def test_start_battle(websocket_client):
    with websocket_client as websocket:
        initial_response = websocket.receive_json()

        websocket.send_text("reset")
        response = websocket.receive_json()

        assert response["log"] != initial_response["log"]
