import pytest
from gameplay import Battle


@pytest.fixture
def battle():
    battle = Battle()
    return battle


def test_attack(battle):
    hero = battle.hero
    villain = battle.villain

    battle.attack(hero, villain)

    assert len(battle.logs) > 0


def test_game_over(battle):
    hero = battle.hero
    villain = battle.villain

    hero.health = 0

    battle.attack(villain, hero)

    assert battle.game_over
    assert len(battle.logs) > 0
    assert battle.logs[-1].startswith("GameOver")


def test_send_logs(battle):
    battle.attack(battle.hero, battle.villain)
    battle.attack(battle.villain, battle.hero)

    logs = battle.send_logs()

    assert "log" in logs
    assert "hero_health" in logs
    assert "villain_health" in logs
    assert "game_over" in logs
