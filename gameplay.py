from characters import Hero, Villain


class Battle:
    def __init__(self):
        self.hero = Hero()
        self.villain = Villain()
        self.turns = 0
        self.logs = []
        self.check_first_turn()
        self.game_over = False

    def check_first_turn(self):
        if self.hero.speed < self.villain.speed:
            self.attack(self.villain, self.hero)
        elif self.hero.speed == self.villain.speed:
            if self.hero.luck < self.villain.luck:
                self.attack(self.villain, self.hero)

    def attack(self, attacker, defender):
        if defender.is_lucky():
            self.logs.append(f"{attacker.name} attacks, but {defender.name} avoids the attack!")
        else:
            damage, modifiers = attacker.calculate_damage(self.logs)
            for mod in range(modifiers):
                defender.take_damage(damage, self.logs)

        self.turns += 1
        self.logs.append("------------------------------")

        if defender.health <= 0 or self.turns >= 20:
            winner = defender.name if defender.health > attacker.health else attacker.name
            self.logs.append(f"GameOver, {winner} won this game, press Reset to Play one more time!")
            self.game_over = True

    def send_logs(self):
        return {
                "log": "\n".join(self.logs),
                "turns": self.turns,
                "hero_health": self.hero.current_health(),
                "villain_health": self.villain.current_health(),
                "hero_stats": self.hero.stats(),
                "villain_stats": self.villain.stats(),
                "game_over": self.game_over,
            }
